<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('add', ['controller' => 'Home', 'action' => 'add']);
$router->add('list', ['controller' => 'Home', 'action' => 'list']);
$router->add('remove', ['controller' => 'Home', 'action' => 'removeList']);
$router->add('edit', ['controller' => 'Home', 'action' => 'editList']);
$router->add('update', ['controller' => 'Home', 'action' => 'updateList']);
$router->add('save', ['controller' => 'Home', 'action' => 'saveList']);
$router->add('{controller}/{action}');
    
$router->dispatch($_SERVER['QUERY_STRING']);
