(function() {
    document.querySelectorAll('.col-action .btn-edit, .col-action .btn-delete')
        .forEach(btn =>
            btn.addEventListener('click', event => {
                event.preventDefault();
                const props = {
                    url: btn.getAttribute('href'),
                };
                const row = btn.closest('tr');
                if (btn.classList.contains('btn-edit')) {
                    console.log('edit');
                    showModal(modalEdit, {
                        ...props,
                        ...getRowProps(row, ['id', 'title', 'description'])
                    });
                } else if (btn.classList.contains('btn-delete')) {
                    console.log('delete');
                    showModal(modalDelete, {
                        ...props,
                        ...getRowProps(row, ['id', 'title'])
                    });
                }
            })
        );
    function getRowProps(row, props) {
        return props.reduce(function(acc, prop) {
            return {
                ...acc,
                [prop]: row.querySelector(`.col-${prop}`).textContent,
            }
        }, {});
    }

    function showModal(modal, props) {
        console.log('showModal', props)
        modal.classList.add('show')
        modal.querySelector('form').setAttribute('action', props.url);
        Object.entries(props).forEach(function(entry) {
            console.log('entry', entry);
            modal.querySelectorAll(`.placeholder-${entry[0]}`).forEach(function(elem) {
                switch (elem.tagName.toLowerCase()) {
                    case 'input':
                        elem.value = entry[1];
                        break;
                    default:
                        elem.innerHTML = entry[1];
                }
            });
        });
    }

    function hideModal(modal) {
        modal.classList.remove('show')
    }

    const modalEdit = document.querySelector('.modal-edit');
    const modalDelete = document.querySelector('.modal-delete');

    document.querySelectorAll('.modal .btn-close').forEach(function(btn) {
       btn.addEventListener('click', function() {
           hideModal(btn.closest('.modal'));
       });
    })
})()