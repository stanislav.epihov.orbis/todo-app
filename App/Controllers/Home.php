<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Task;
use \Core\Redirect;

/**
 * Home controller
 */
class Home extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('Home/index.html');
    }

    public function add()
    {
        View::renderTemplate('Home/add.html');
    }

    public function saveList()
    {
        $inputs = [];
        if(isset($_POST["title"]) && $_POST["title"] != ''){
            $inputs['title'] = $_POST["title"];
            $inputs['description'] = $_POST["description"];

            if(Task::save($inputs)){
                Redirect::to('list', false);
            } else {
                throw new \Exception("Error save data");
            }
        } else {
            throw new \Exception("Input tittle is required");
        }

    }

    public function list()
    {
        $lists = Task::getAll();
        View::renderTemplate('Home/list.html',['lists' => $lists]);
    }

    public function removeList()
    {
        if (isset($_GET["id"]) && is_numeric( $_GET['id'] ) && $_GET['id'] > 0 ) {

            if (Task::delete($_GET["id"])) {
                Redirect::to('list', false);
            } else {
                throw new \Exception("Error delete record");
            }
        }else {
            throw new \Exception("No id to delete");
        }
    }

    public function editList()
    {
        if (isset($_GET["id"]) && is_numeric( $_GET['id'] ) && $_GET['id'] > 0 ) {

            if ($list = Task::getList($_GET["id"])) {
                View::renderTemplate('Home/edit.html',['list' => $list]);
            } else {
                throw new \Exception("Error get record");
            }
        }else {
            throw new \Exception("No id to edit");
        }
    }

    public function updateList()
    {
        $inputs = [];
        if(isset($_POST["title"]) && isset($_POST["description"]) && isset($_POST["id"])){
            $inputs['title'] = $_POST["title"];
            $inputs['description'] = $_POST["description"];
            $inputs['id'] = $_POST["id"];

            if (Task::edit($inputs)) {
                Redirect::to('list', false);
            } else {
                throw new \Exception("Error update record");
            }
        }else{
            throw new \Exception("Input tittle is required");
        }




    }
}
