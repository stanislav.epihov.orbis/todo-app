<?php

namespace App\Models;

use PDO;

class Task extends \Core\Model
{
    /**
     * Get all the records as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT id, title, description, date_created FROM tasks ORDER BY id DESC');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Get all the records as an associative array
     *
     * @return array
     */
    public static function getList($id)
    {
        $db = static::getDB();
        $statement = $db->prepare("SELECT id, title, description, date_created FROM tasks WHERE id=? LIMIT 1");
        $statement->execute([$id]);
        $row = $statement->fetch();

        return $row;
    }

    /**
     * Seve data
     *
     * @return mixed
     */
    public static function save($data)
    {
        $db = static::getDB();

        $sql = 'INSERT INTO tasks(title, description, date_created) VALUES(:title, :description, :date_created)';

        $statement = $db->prepare($sql);
        $statement->execute([
            ':title' => trim($data['title']),
            ':description' => trim($data['description']),
            ':date_created' => date('Y-m-d H:i:s')

        ]);

        $publisher_id = $db->lastInsertId();

        return $publisher_id;
    }

    /**
     * delete record
     *
     * @return mixed
     */
    public static function delete($id)
    {
        $db = static::getDB();

        $stmt = $db->prepare( "DELETE FROM tasks WHERE id =:id" );
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        if( ! $stmt->rowCount() ){
            return false;
        } else{
            return true;
        }

    }

    /**
     * edit record
     *
     * @return mixed
     */
    public static function edit($data)
    {
        $db = static::getDB();

        $sql = "UPDATE tasks SET title=:title, description=:description WHERE id=:id";

        $statement = $db->prepare($sql);
        if($statement->execute([
            'title' => trim($data['title']),
            'description' => trim($data['description']),
            'id' => trim($data['id'])
        ])){
            return true;
        }else{
            return false;
        }
    }
}
